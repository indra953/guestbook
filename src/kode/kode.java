/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kode;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author W1716
 */
public class kode {
   private ResultSet rs;
    private Connection cn;
    private Statement st;

    public kode()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error inisialisasi driver");
        }

        try
        {
            st = DriverManager.getConnection("jdbc:mysql://localhost/bankpbosesi12","root","").createStatement();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error koneksi ke database");
        }
    }

    public void insertData(String data)
    {
        try
        {
            st.executeUpdate(data);
            JOptionPane.showMessageDialog(null, "Data berhasil di simpan");
            
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Gagal Insert Data");
        }
    }

    public ResultSet lihatData(String a)
    {
        try
        {
            rs = st.executeQuery(a);
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Gagal Lihat Data");
        }
        return rs;
    }

    
}
